# Titre de la Merge Request

## Tâches/Issues associées

Liste des tâches/issues associés à cette Merge Request :

* #1

## Principaux changements

Cette Merge Request permet de traquer les changements à apporter après l'install du prochain package.

* Changement 1

## Remarques et commentaires

Ajouter ici des eventuels remarques ou commentaires au sujet de votre Merge Request
